__author__ = 'gemini'

# LICENSED UNDER GPL-3
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from urllib2 import urlopen
from bs4 import BeautifulSoup

class TPBScraper(object):
    '''
    The Pirate Bay torrent website scraper.
    Works the same as the Nyaa torrent scraper, load the URL with urlopen then BeautifulSoup parses it.
    Can't use SoupStrainer here, though.
    This makes this scraper *slower* than the other one(s) since it's going to parse all the website before getting to the wanted tags.
    '''
    def __init__(self, *args):
        self.tpb_url = 'https://thepiratebay.cr/search/%s/0/99/0' % args
        self.open_url = urlopen(self.tpb_url)
        self.soup = BeautifulSoup(self.open_url)

    def scrap_name(self):
        names = []
        for item in self.soup.find_all('div', {'class': 'detName'}):
            names.append(item.get_text())
        return names

    def scrap_desc(self):
        descs = []
        for item in self.soup.find_all('font', {'class': 'detDesc'}):
            descs.append(item.get_text())
        return descs

    def scrap_dl(self):
        dls = []
        for link in self.soup.find_all('a'):
            if link['href'].startswith('/torrent/'):
                url = 'https://thepiratebay.cr'
                dls.append(url + link['href'])
            else:
                pass
        return dls

    def scrap_magnets(self):
        magnets = []
        for magnet in self.soup.find_all('a'):
            if magnet['href'].startswith('magnet:?xt=urn'):
                magnets.append(magnet['href'])
            else:
                pass
        return magnets

    # TODO: Scrap seeders and leechers from the website
    def scrap_seeders(self):
        pass

    def scrap_leechers(self):
        pass
    

# data = TPBScraper('ghost in the shell')
# print data.scrap_name()[0]
# from subprocess import call
# from sys import stdout
# item = lambda x, y: stdout.write('Downloading ' + x + ' using ' + y + '\n')
# print "Want to download it? [y/N]"
# klovs = raw_input('> ')
# if not klovs:
#     raise AttributeError('Expected y[yes] or n[no]')
# elif klovs == 'deluge':
#     call(['deluge', '%s']) % data.scrap_magnets()[0]
#     item(data.scrap_name()[0], 'Deluge')
# elif klovs.lower() == 'transmission':
#     item(data.scrap_name()[0], 'Tixati')
#     call(['transmission-qt %s' '&']) % data.scrap_magnets()[0]
