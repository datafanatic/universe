__author__ = 'gemini'
# Copyright (c) 2015 Jonathan Blake
# LICENSED UNDER GPLv3
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from requests import get
from bs4 import BeautifulSoup, SoupStrainer

class NyaaScraper:
    """
    Nyaa (nyaa.se) website scraper.
    This scraper, differently from TPB scraper, only parses the 'td' tags since the HTML is very well formatted in this website.
    Every attribute is scraped, stored in a list then returned at the end of the function.
    Every new feature should be added as a new function.
    """
    def __init__(self, *args):

        self.url = "http://www.nyaa.se/?page=search&term=%s&sort=2" % args
        self.open_url = get(self.url)
        self.soup = BeautifulSoup(self.open_url.text, parse_only=SoupStrainer('td'))

    def scrap_episode_name(self):
        names = []
        table = self.soup.find_all('td', {'class': 'tlistname'})
        for name in table:
            names.append(name.get_text())
        return names

    def scrap_episode_size(self):
        sizes = []
        for file_size in self.soup.find_all('td', {'class': 'tlistsize'}):
            sizes.append(file_size.get_text())
        return sizes

    def scrap_episode_link(self):
        links = []
        data = self.soup.findAll('td', attrs={'class': 'tlistdownload'})
        for div in data:
            links_d = div.find_all('a')
            for link in links_d:
                links.append(link['href'])
        return links

    def scrap_episode_seeds(self):
        seeds = []
        for seed in self.soup.find_all('td', {'class': 'tlistsn'}):
            seeds.append(seed.get_text())
        return seeds

    def scrap_episode_leeches(self):
        leechers = []
        for leech in self.soup.find_all('td', {'class': 'tlistln'}):
            leechers.append(leech.get_text())
        return leechers

    def get_upload_date(self, *urls):
        #
        # This function runs for every link found in scrap_episode_link() as the date in which the torrent was sent is only found there.
        # This function is *fast* (as it can be since it is parsing possibly hundreds of urls) as it only needs one element from the page.
        # Gets a list (generally from get_absolute_url()) then returns the date in which the torrent was uploaded.
        #
        # TODO: Find a way to better implement it. This seems to be pretty slow.
        # TODO: Implement multi-thread, thus possibly making the function faster.
        dates = []
        for url in urls:
            ep_url = get(url)
            ep_soup = BeautifulSoup(ep_url.text, parse_only=SoupStrainer('td'))
            for date in ep_soup.find('td', attrs={'class': 'vtop'}):
                dates.append(date)
        return dates

    def get_absolute_url(self):
        absolute_urls = []
        for absolute_link in self.soup.find_all('td', {'class': 'tlistname'}):
            for link in absolute_link.find_all('a'):
                if 'http://www.nyaa.se/?page=view&tid=' in link['href']:
                    absolute_urls.append(link['href'])
                else:
                    pass
        return absolute_urls


