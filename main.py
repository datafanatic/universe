__author__ = 'gemini'
# -*- encoding: utf-8 -*-
# Copyright (c) 2015 Jonathan Blake
# LICENSED UNDER GPLv3
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from flask import Flask, render_template, request
import sqlite3
from collections import namedtuple
from nyaa import NyaaScraper

app = Flask(__name__)

DATABASE = 'queries.db'

def insert(where, query, select_what):
    with sqlite3.connect(DATABASE) as db:
        cursor = db.cursor()
        times = cursor.execute("SELECT %s FROM %s" % (select_what, where))
        cursor.execute("INSERT INTO %s VALUES('%s', '%s')" % (where, query, times))
        db.commit()

def get_upload_date_on_nyaa():
    dates = []
    parsed_query = request.form['search'].replace('-nyaa', '')
    date = NyaaScraper(parsed_query)
    for item in date.get_absolute_url():
        dates.append(date.get_upload_date(item))
    return dates
def run_search_on_nyaa():
    # Done: Implement this as a namedtuple instead of this zip() mess.
    parsed_query = request.form['search'].replace('-nyaa', '')
    search_on_nyaa = NyaaScraper(parsed_query)
    fields = namedtuple('Fields', 'name link size seeds leeches date')
    data = fields(name=search_on_nyaa.scrap_episode_name(),
                  size=search_on_nyaa.scrap_episode_size(),
                  link=search_on_nyaa.scrap_episode_link(),
                  seeds=search_on_nyaa.scrap_episode_seeds(),
                  leeches=search_on_nyaa.scrap_episode_leeches(),
                  date=get_upload_date_on_nyaa())
    insert('NyaaQueries', parsed_query.lower(), 'Times')
    return zip(data.name,
               data.size,
               data.link,
               data.seeds,
               data.leeches,
               data.date)

@app.route("/", methods=['GET', 'POST'])
def search_page():
    return render_template('index.html')

def run_queries():
    return run_search_on_nyaa()

@app.route("/search", methods=['GET', 'POST'])
def run_search():
    return render_template('search.html', nyaa_query=run_search_on_nyaa(),
                           kat_query=run_search_on_kat(),
                           tpb_query=run_search_on_tpb())

@app.route("/search", methods=['GET', 'POST'])
def run_search_on_kat():
    from KickassAPI import Search
    items = []
    search = Search(request.form['search'].replace('-kat', ''))
    for item in search:
        items.append(item.lookup())
    return items

@app.route('/search', methods=['GET', 'POST'])
def run_search_on_tpb():
    from ThePirateBay import TPBScraper
    search = TPBScraper(request.form['search'].replace('-tpb', ''))
    fields = namedtuple('Fields', 'name magnet link')
    tpb_fields = fields(name=search.scrap_name(),
                        magnet=search.scrap_magnets(),
                        link=search.scrap_dl())
    return zip(tpb_fields.name,
               tpb_fields.magnet,
               tpb_fields.link)

if __name__ == '__main__':
    app.run(debug=True)
